package view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import control.PessoaControl;
import control.ProdutoControl;
import entity.EspecificacaoPessoa;
import entity.EspecificacaoProduto;

public class FormCadastroPessoa extends JDialog {

	private JPanel panel = new JPanel();
	private JTextField login = new JTextField(10);
	private JPasswordField senha = new JPasswordField(10);
	private JRadioButton admin = new JRadioButton("Administrador");
	private JRadioButton vendedor = new JRadioButton("Vendedor");
	private JRadioButton cliente = new JRadioButton("Cliente");
	private JTextField dataFinal = new JTextField(10);
	private JTextField nome = new JTextField(20);
	private JTextField cpf = new JTextField(20);
	private JTextField limite = new JTextField(10);
	private JTextField dataNascimento = new JTextField(10);
	private JTextField rua = new JTextField(20);
	private JTextField bairro = new JTextField(20);
	private JTextField cidade = new JTextField(10);
	private JTextField estado = new JTextField(10);
	private JTextField numero = new JTextField(10);
	private JButton botao = new JButton("Gravar");
	private ButtonGroup cargo = new ButtonGroup();

	public static void main(String[] args) {
		FormCadastroPessoa formCadUsuario = new FormCadastroPessoa();
	}

	public FormCadastroPessoa() {
		this.panel.setLayout(new FlowLayout(FlowLayout.LEFT, 150, 10));
		this.panel.setPreferredSize(new Dimension(300, 200));
		this.setSize(640, 480);
		this.setTitle("Form Cadastro Usuario v0.000001");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		JLabel jNome = new JLabel("Nome");
		this.panel.add(jNome);
		this.panel.add(nome);

		JLabel jCpf = new JLabel("CPF   ");
		this.panel.add(jCpf);
		this.panel.add(cpf);

		JLabel jLimiteCredito = new JLabel("Limite de Credito      ");
		this.panel.add(jLimiteCredito);
		this.panel.add(limite);

		JLabel jDataNascimento = new JLabel("Data de Nascimento");
		this.panel.add(jDataNascimento);
		this.panel.add(dataNascimento);

		JLabel jRua = new JLabel("Rua   ");
		this.panel.add(jRua);
		this.panel.add(rua);

		JLabel jCidade = new JLabel("Cidade");
		this.panel.add(jCidade);
		this.panel.add(cidade);

		JLabel jBairro = new JLabel("Bairro ");
		this.panel.add(jBairro);
		this.panel.add(bairro);

		JLabel jEstado = new JLabel("Estado ");
		this.panel.add(jEstado);
		this.panel.add(estado);

		JLabel jNumero = new JLabel("Numero");
		this.panel.add(jNumero);
		this.panel.add(numero);

		JLabel jlLogin = new JLabel("Login  ");
		this.panel.add(jlLogin);
		this.panel.add(login);

		JLabel jlSenha = new JLabel("Senha ");
		this.panel.add(jlSenha);
		this.panel.add(senha);

		this.panel.add(this.admin);
		this.cargo.add(admin);
		this.admin.setActionCommand("Administrador");

		this.panel.add(this.vendedor);
		this.cargo.add(vendedor);
		this.vendedor.setActionCommand("Vendedor");
		
		this.panel.add(this.cliente);
		this.cargo.add(cliente);
		this.cliente.setActionCommand("Cliente");

		
		JLabel jlDataFinal = new JLabel("Data de Expiracao");
		this.panel.add(jlDataFinal);
		this.panel.add(dataFinal);
		this.panel.add(botao);

		this.add(this.panel, BorderLayout.CENTER);
		this.botao.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EspecificacaoPessoa especPessoa = new EspecificacaoPessoa();

				PessoaControl pessoaControl = new PessoaControl();
				especPessoa.setLogin(login.getText());
				especPessoa.setSenha(senha.getPassword());
				especPessoa.setCargo(cargo.getSelection().getActionCommand());
				especPessoa.setDataInicial();

				especPessoa.setDataFinal(dataFinal.getText());
				especPessoa.setNome(nome.getText());
				especPessoa.setCpf(cpf.getText());
				especPessoa.setLimite(Integer.valueOf(limite.getText()));

				especPessoa.setRua(rua.getText());
				especPessoa.setDataNascimento(dataNascimento.getText());
				especPessoa.setCidade(cidade.getText());
				especPessoa.setBairro(bairro.getText());
				especPessoa.setEstado(estado.getText());
				especPessoa.setNumero(Integer.valueOf(numero.getText()));
				try {
					pessoaControl.setNovoPessoa(especPessoa);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				JOptionPane.showMessageDialog(null, "Gravou!");

				for (int i = 0; i < getContentPane().getComponentCount(); i++) {
					Component c = getContentPane().getComponent(i);
					if (c instanceof JTextField) {
						JTextField field = (JTextField) c;
						field.setText(null);
					}
				}
			}
		});
	}
}
