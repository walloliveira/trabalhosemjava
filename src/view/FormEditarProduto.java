package view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;

import control.ProdutoControl;
import control.ResultSetTableModel;

import entity.EspecificacaoProduto;
import entity.Produto;
import javax.swing.SwingConstants;

public class FormEditarProduto extends JDialog{
private JTextField codigo = new JTextField(5);
private JTextField descricao  = new JTextField(20);
private JTextField codigoDeBarra = new JTextField(10);
private JTextField valor = new JTextField(10);
private JButton salvar = new JButton("Salvar");
private JButton cancelar = new JButton("Cancelar");
private JScrollPane jScrollPanel = new JScrollPane();
private ProdutoControl produtoControl = new ProdutoControl();

	public FormEditarProduto(EspecificacaoProduto espec){
		this.setSize(480,360);
		this.setTitle("Form Cadastro Produto v0.000001");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		JLabel jlCodigo = new JLabel("Codigo:");
		jlCodigo.setBounds(122, 19, 54, 15);
		getContentPane().add(jlCodigo);
		this.codigo.setBounds(182, 17, 59, 19);
		this.codigo.setText(String.valueOf(espec.getId()));
		this.codigo.setHorizontalAlignment(SwingConstants.LEFT);
		getContentPane().add(this.codigo);
		this.codigo.disable();
		JLabel jlCodigoDeBarra = new JLabel("Codigo De Barra:");
		jlCodigoDeBarra.setBounds(54, 81, 120, 15);
		getContentPane().add(jlCodigoDeBarra);
		this.descricao.setBounds(182, 48, 114, 19);
		this.descricao.setText(espec.getDescricao());
		getContentPane().add(this.descricao);
		this.cancelar.setBounds(104, 156, 96, 25);
		getContentPane().add(this.cancelar);
		this.salvar.setBounds(326, 156, 78, 25);
		getContentPane().add(this.salvar);
		JLabel jlDescricao = new JLabel("Descrição:");
		jlDescricao.setBounds(104, 50, 74, 15);
		getContentPane().add(jlDescricao);
		this.valor.setBounds(182, 110, 114, 19);
		this.valor.setText(String.valueOf(espec.getValor()));
		getContentPane().add(this.valor);
		this.codigoDeBarra.setBounds(180, 79, 224, 19);
		this.codigoDeBarra.setText(espec.getCodigoDeBarra());
		getContentPane().add(this.codigoDeBarra);
		JLabel jlValor = new JLabel("Valor:");
		jlValor.setBounds(132, 112, 42, 15);
		getContentPane().add(jlValor);
		this.jScrollPanel.setBounds(238, 137, 3, 3);
		getContentPane().add(this.jScrollPanel);
		
		this.salvar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				EspecificacaoProduto novaEspec = new EspecificacaoProduto();
				novaEspec.setId(Integer.parseInt(codigo.getText()));
				novaEspec.setDescricao(descricao.getText());
				novaEspec.setCodigoDeBarra(codigoDeBarra.getText());
				novaEspec.setValor(new BigDecimal(valor.getText()));
				
				try {
					produtoControl.editaProduto(novaEspec);
					JOptionPane.showMessageDialog(null, "Gravou!");
					dispose();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		this.cancelar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

}
