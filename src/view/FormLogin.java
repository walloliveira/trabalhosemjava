package view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import control.PessoaControl;
import entity.EspecificacaoPessoa;


public class FormLogin extends JDialog {
	private JPanel panel = new JPanel();
	private JTextField usuario = new JTextField(10);
	private JPasswordField senha = new JPasswordField(20);
	private JButton login = new JButton("Logar");

	public FormLogin() {
		this.setSize(300, 200);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		panel.setLayout(null);
		JLabel jlabel = new JLabel("Login:");
		jlabel.setBounds(54, 25, 44, 15);
		this.panel.add(jlabel);
		usuario.setBounds(104, 23, 114, 19);
		this.panel.add(this.usuario);
		JLabel jLabel2 = new JLabel("Senha: ");
		jLabel2.setBounds(44, 69, 54, 15);
		this.panel.add(jLabel2);
		senha.setBounds(104, 67, 114, 19);
		this.panel.add(this.senha);
		login.setBounds(126, 139, 75, 25);
		this.panel.add(login);
		getContentPane().add(this.panel);
		this.login.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				EspecificacaoPessoa especUsuario = new EspecificacaoPessoa();
				PessoaControl usuarioControl = new PessoaControl();
				especUsuario.setLogin(usuario.getText());
				especUsuario.setSenha(senha.getPassword());
				try {
					Formulario formulario = new Formulario(usuarioControl.validaLogin(especUsuario));
					formulario.setVisible(true);
					dispose();
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null, "Usuario ou senha incorretos! Verifique novamente!");
					e1.printStackTrace();
				}
				
			}
		});
	}
}
