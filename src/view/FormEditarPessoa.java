package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import control.PessoaControl;
import control.ResultSetTableModel;

import entity.EspecificacaoPessoa;
import entity.EspecificacaoProduto;

public class FormEditarPessoa extends JDialog {

	private JPanel panel = new JPanel();
	private JRadioButton admin = new JRadioButton("Administrador");
	private JRadioButton vendedor = new JRadioButton("Vendedor");
	private JRadioButton cliente = new JRadioButton("Cliente");
	private JTextField login = new JTextField(10);
	private JPasswordField senha = new JPasswordField(10);
	private JTextField dataFinal = new JTextField(10);
	private JTextField nome = new JTextField(20);
	private JTextField cpf = new JTextField(20);
	private JTextField limite = new JTextField(10);
	private JTextField dataNascimento = new JTextField(10);
	private JTextField rua = new JTextField(20);
	private JTextField bairro = new JTextField(20);
	private JTextField cidade = new JTextField(10);
	private JTextField estado = new JTextField(10);
	private JTextField numero = new JTextField(10);
	private ButtonGroup cargo = new ButtonGroup();
	private JButton salvar = new JButton("Salvar");
	private JButton cancelar = new JButton("Cancelar");
	private JScrollPane jScrollPanel = new JScrollPane();
	PessoaControl pessoaControl = new PessoaControl();
	private ResultSetTableModel tableModel = new ResultSetTableModel();

	public FormEditarPessoa(final EspecificacaoPessoa espec) {
		this.panel.setLayout(new FlowLayout(FlowLayout.LEFT, 150, 10));
		this.panel.setPreferredSize(new Dimension(300, 200));
		this.setSize(640, 480);
		this.setTitle("Form Edita Usuario v0.000001");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		JLabel jNome = new JLabel("Nome");
		this.panel.add(jNome);
		this.panel.add(nome);

		JLabel jCpf = new JLabel("CPF   ");
		this.panel.add(jCpf);
		this.panel.add(cpf);

		JLabel jLimiteCredito = new JLabel("Limite de Credito      ");
		this.panel.add(jLimiteCredito);
		this.panel.add(limite);

		JLabel jDataNascimento = new JLabel("Data de Nascimento");
		this.panel.add(jDataNascimento);
		this.panel.add(dataNascimento);

		JLabel jRua = new JLabel("Rua   ");
		this.panel.add(jRua);
		this.panel.add(rua);

		JLabel jCidade = new JLabel("Cidade");
		this.panel.add(jCidade);
		this.panel.add(cidade);

		JLabel jBairro = new JLabel("Bairro ");
		this.panel.add(jBairro);
		this.panel.add(bairro);

		JLabel jEstado = new JLabel("Estado ");
		this.panel.add(jEstado);
		this.panel.add(estado);

		JLabel jNumero = new JLabel("Numero");
		this.panel.add(jNumero);
		this.panel.add(numero);

		JLabel jlLogin = new JLabel("Login  ");
		this.panel.add(jlLogin);
		this.panel.add(login);

		JLabel jlSenha = new JLabel("Senha ");
		this.panel.add(jlSenha);
		this.panel.add(senha);

		this.panel.add(this.admin);
		this.cargo.add(admin);
		this.admin.setActionCommand("Administrador");

		this.panel.add(this.vendedor);
		this.cargo.add(vendedor);
		this.vendedor.setActionCommand("Vendedor");

		this.panel.add(this.cliente);
		this.cargo.add(cliente);
		this.cliente.setActionCommand("Cliente");

		this.salvar.setBounds(326, 400, 78, 25);
		getContentPane().add(this.salvar);
		this.add(this.panel, BorderLayout.CENTER);
		this.salvar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EspecificacaoPessoa novaEspec = new EspecificacaoPessoa();
				novaEspec.setLogin(login.getText());
				novaEspec.setSenha(senha.getPassword());
				novaEspec.setCargo(cargo.getSelection().getActionCommand());
				novaEspec.setDataInicial();

				novaEspec.setDataFinal(dataFinal.getText());
				novaEspec.setNome(nome.getText());
				novaEspec.setCpf(cpf.getText());
				novaEspec.setLimite(Integer.valueOf(limite.getText()));

				novaEspec.setRua(rua.getText());
				novaEspec.setDataNascimento(dataNascimento.getText());
				novaEspec.setCidade(cidade.getText());
				novaEspec.setBairro(bairro.getText());
				novaEspec.setEstado(estado.getText());
				novaEspec.setNumero(Integer.valueOf(numero.getText()));
				novaEspec.setParametro(espec);
				try {
					pessoaControl.editaPessoa(novaEspec);
					JOptionPane.showMessageDialog(null, "Gravou!");
					dispose();
				} catch (SQLException e1) {
					e1.printStackTrace();
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
			}
		});
		this.cancelar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

}
