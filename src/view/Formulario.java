package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import control.PessoaControl;
import entity.Pessoa;

public class Formulario extends JDialog {
	public Formulario(Pessoa pessoa) {
		this.setSize(640, 480);
		this.setTitle("Form principal v0.000001");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		abrirMenuPrincipal(pessoa);
	}

	private void abrirMenuPrincipal(Pessoa pessoa) {
		JMenu mnCadastro = new JMenu("Cadastro");
		mnCadastro.setMnemonic('C');

		JMenuItem itemProduto = new JMenuItem("Produto");
		itemProduto.setMnemonic('P');
		
		itemProduto.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FormListarProduto formuCadastroProduto = new FormListarProduto();
				formuCadastroProduto.setVisible(true);
			}
		});
		
		mnCadastro.add(itemProduto);

		JMenuItem itemPessoa = new JMenuItem("Usuario");
		itemPessoa.setMnemonic('U');
		itemPessoa.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				FormCadastroPessoa formCadastroPessoa = new FormCadastroPessoa();
				formCadastroPessoa.setVisible(true);
			}
		});
		mnCadastro.add(itemPessoa);
		
		JMenuItem editaPessoa = new JMenuItem("Edita Usuario");
		editaPessoa.setMnemonic('E');
		editaPessoa.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Editar ed = new Editar();
				ed.setVisible(true);
			}
		});
		mnCadastro.add(editaPessoa);
		
		JMenuItem deletaPessoa = new JMenuItem("Deleta Usuario");
		deletaPessoa.setMnemonic('D');
		deletaPessoa.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				FormDeletaPessoa deleta = new FormDeletaPessoa();
				deleta.setVisible(true);
			}
		});
		mnCadastro.add(deletaPessoa);
		JMenu pedidoVenda = new JMenu("Venda");
		pedidoVenda.setMnemonic('V');
		
		JMenuItem itemPedidoVenda = new JMenuItem("Movimento Pedido de Venda");
		itemPedidoVenda.setMnemonic('P');
		
		itemPedidoVenda.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		pedidoVenda.add(itemPedidoVenda);

		JMenuBar barraDeOpcoes = new JMenuBar();
		
		setJMenuBar(barraDeOpcoes);
		
		JMenu relatorio = new JMenu("Relatorio");
		relatorio.setMnemonic('V');
		
		JMenuItem itemRelatorio = new JMenuItem("Pessoa");
		itemRelatorio.setMnemonic('P');
		itemRelatorio.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				FormListarPessoa formPessoa = new FormListarPessoa();
				formPessoa.setVisible(true);
			}
		});
		relatorio.add(itemRelatorio);
		
		PessoaControl pessoaControl = new PessoaControl();
		
		

		try {
			if(pessoaControl.retornaNivelDeAcesso(pessoa).equals("Vendedor")){
				barraDeOpcoes.add(pedidoVenda);
			} else {
				barraDeOpcoes.add(mnCadastro);
				barraDeOpcoes.add(pedidoVenda);
				barraDeOpcoes.add(relatorio);
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		getContentPane();
		setVisible(true);
	}
}