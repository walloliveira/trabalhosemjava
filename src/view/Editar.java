package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import control.PessoaControl;

import entity.EspecificacaoPessoa;
import entity.Pessoa;

public class Editar extends JDialog {
	private JTextField cpf = new JTextField(20);
	private JPanel panel = new JPanel();
	private JButton botao = new JButton("Editar");

	public Editar() {
		this.panel.setLayout(new FlowLayout(FlowLayout.LEFT, 150, 10));
		this.panel.setPreferredSize(new Dimension(300, 200));
		this.setSize(480, 240);
		this.setTitle("Form Editar");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		JLabel jCpf = new JLabel("Digite o CPF para Editar");
		this.panel.add(jCpf);
		this.panel.add(cpf);
		this.panel.add(botao);

		this.add(this.panel, BorderLayout.CENTER);
		this.botao.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				EspecificacaoPessoa espec = new EspecificacaoPessoa();
				PessoaControl pc = new PessoaControl();
				espec.setCpf(cpf.getText());
				Pessoa pessoa = new Pessoa();
				pessoa.setEspecificacao(espec);
				try {
					pessoa = pc.buscaUsuario(pessoa);
					if(pessoa != null){
						FormEditarPessoa edita = new FormEditarPessoa(espec);
						edita.setVisible(true);
					}else{
						JOptionPane.showMessageDialog(null, "Nao existe Pessoa com esse CPF");
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
		});
	}
}