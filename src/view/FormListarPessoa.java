package view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.SQLException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;

import org.postgresql.util.PSQLException;

import control.ResultSetTableModel;
import entity.EspecificacaoPessoa;
import entity.EspecificacaoProduto;
import control.PessoaControl;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class FormListarPessoa extends JDialog {
	private JScrollPane jScrollPanel = new JScrollPane();
	private ResultSetTableModel tableModel = new ResultSetTableModel();

	public FormListarPessoa() {
		this.setSize(480,360);
		this.setTitle("Form Listar Pessoa v0.000001");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.tableModel.listarPessoa();
		getContentPane().setLayout(null);
		JTable jt = new JTable(this.tableModel);
		jt.setFillsViewportHeight(true);
		jt.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
		jt.setColumnModel(jt.getColumnModel());
		jt.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jt.setSize(new Dimension(500, 500));
		this.jScrollPanel.add(jt);
		this.jScrollPanel.setBounds(12, 12, 453, 253);
		this.jScrollPanel.setColumnHeaderView(jt);
		this.jScrollPanel.setRowHeaderView(jt);
		getContentPane().add(this.jScrollPanel);
	}
}
