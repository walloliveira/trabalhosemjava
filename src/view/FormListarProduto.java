package view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;

import org.postgresql.util.PSQLException;

import control.ProdutoControl;
import control.ResultSetTableModel;
import entity.EspecificacaoProduto;
import entity.Produto;

public class FormListarProduto extends JDialog{
	JScrollPane jScrollPanel = new JScrollPane();
	private JTextField codigo = new JTextField(5);
	private JTextField descricao = new JTextField(20);
	private JTextField codigoDeBarra = new JTextField(10);
	private JTextField valor = new JTextField(10);
	private JButton incluir = new JButton("Incluir");
	private JButton deletar = new JButton("Deletar");
	private JButton editar = new JButton("Editar");
	private ProdutoControl produtoControl = new ProdutoControl();
	private JTable jt;
	
	public FormListarProduto(){
		this.setSize(480,360);
		this.setTitle("Form Cadastro Produto v0.000001");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		JLabel jlCodigo = new JLabel("Codigo:");
		jlCodigo.setBounds(110, 14, 54, 15);
		getContentPane().add(jlCodigo);
		codigo.setBounds(169, 12, 59, 19);
		getContentPane().add(this.codigo);
		JLabel jlDescricao = new JLabel("Descrição:");
		jlDescricao.setBounds(90, 41, 74, 15);
		getContentPane().add(jlDescricao);
		descricao.setBounds(169, 43, 224, 19);
		getContentPane().add(this.descricao);
		JLabel jlCodigoDeBarra = new JLabel("Codigo De Barra:");
		jlCodigoDeBarra.setBounds(44, 70, 120, 15);
		getContentPane().add(jlCodigoDeBarra);
		codigoDeBarra.setBounds(169, 68, 114, 19);
		getContentPane().add(this.codigoDeBarra);
		JLabel jlValor = new JLabel("Valor:");
		jlValor.setBounds(122, 99, 42, 15);
		this.produtoControl.getTableModel().setQuery();
		getContentPane().add(jlValor);
		valor.setBounds(169, 97, 114, 19);
		getContentPane().add(this.valor);
		incluir.setBounds(63, 140, 77, 25);
		getContentPane().add(this.incluir);
		editar.setBounds(179, 140, 77, 25);
		getContentPane().add(this.editar);
		deletar.setBounds(293, 140, 92, 25);
		getContentPane().add(this.deletar);
		jt = new JTable(this.produtoControl.getTableModel());
		jt.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jt.setBorder(UIManager.getBorder("Table.scrollPaneBorder"));
		this.jt.setSize(new Dimension(100,100));
		jScrollPanel.setViewportBorder(UIManager.getBorder("Table.scrollPaneBorder"));
		jScrollPanel.setBounds(12, 177, 453, 155);
		jScrollPanel.add(jt);
		jScrollPanel.setRowHeaderView(this.jt);
		jScrollPanel.setColumnHeaderView(this.jt);
		getContentPane().add(jScrollPanel);
		
		this.incluir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				EspecificacaoProduto especProd = new EspecificacaoProduto();
				
				especProd.setId(Integer.parseInt(codigo.getText()));
				especProd.setDescricao(descricao.getText());
				especProd.setCodigoDeBarra(codigoDeBarra.getText());
				especProd.setValor(new BigDecimal(valor.getText()));
				
				try {
					produtoControl.incluirNovoProduto(especProd);
					JOptionPane.showMessageDialog(null, "Gravou!");
					produtoControl.getTableModel().setQuery();
					
					for (int i = 0; i < getContentPane().getComponentCount(); i++){
						Component c = getContentPane().getComponent(i);
						if(c instanceof JTextField){
							JTextField field = (JTextField)c;
							field.setText(null);
						}
					}
					getRootPane().setDefaultButton(incluir);
					codigo.requestFocus();

				} catch (PSQLException ex) {
					JOptionPane.showMessageDialog(null, "Produto ja cadastrado!");
					ex.printStackTrace();
				} catch (SQLException b) {
					JOptionPane.showMessageDialog(null, "Não foi possível cadastrar o produto!");
					JOptionPane.showMessageDialog(null, "Nao foi possivel cadastrar o produto!");
					b.printStackTrace();
				}
			}
		});
		
		this.editar.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				if(jt.getSelectedRow() != -1){
					EspecificacaoProduto espec = new EspecificacaoProduto();
					espec.setId((Integer)(jt.getValueAt(jt.getSelectedRow(),0)));
					espec.setDescricao((String)(jt.getValueAt(jt.getSelectedRow(), 1)));
					espec.setCodigoDeBarra((String)(jt.getValueAt(jt.getSelectedRow(), 2)));
					espec.setValor((BigDecimal)(jt.getValueAt(jt.getSelectedRow(), 3)));
					FormEditarProduto editaProduto = new FormEditarProduto(espec);
					editaProduto.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Selecione uma linha no JTable!");
				}
			}
		});
		
		this.deletar.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				if(jt.getSelectedRow() != -1){
					EspecificacaoProduto espec = new EspecificacaoProduto();
					espec.setId((Integer)(jt.getValueAt(jt.getSelectedRow(),0)));
					try {
						produtoControl.deletarProduto(espec);
						JOptionPane.showMessageDialog(null, "Produto deletado!");
						produtoControl.getTableModel().setQuery();
					} catch (SQLException e1) {
						JOptionPane.showMessageDialog(null, "Produto nao encontrado!");
					}
				} else {
					JOptionPane.showMessageDialog(null, "Selecione uma linha no JTable!");
				}
			}
		});
	}
}
