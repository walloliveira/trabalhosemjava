package entity;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Especificacao {
	private int id;
	private String descricao;
	private String codigoDeBarra;
	private BigDecimal valor;
	private String login;
	private char[] senha;
	private String cargo;
	private String dataInicial;
	private String dataFinal;

	public String getLogin() {
		return login;
	}

	public char[] getSenha() {
		return senha;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setSenha(char[] cs) {
		this.senha = cs;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setCodigoDeBarra(String codigoDeBarra) {
		this.codigoDeBarra = codigoDeBarra;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public int getId() {
		return this.id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public String getCodigoDeBarra() {
		return this.codigoDeBarra;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public String getDataFinal() {
		return dataFinal;
	}

	public void setDadaFinal(String dataFinal) {

		this.dataFinal = dataFinal;
	}

	public String getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial() {
		DateFormat df = new SimpleDateFormat("DD/MM/YYYY");
		this.dataInicial = df.format(new Date());
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
}
