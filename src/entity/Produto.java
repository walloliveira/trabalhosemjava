package entity;

import java.math.BigDecimal;

public class Produto {
	private EspecificacaoProduto especificacao;

	public EspecificacaoProduto getEspecificacao() {
		return especificacao;
	}

	public void setEspecificacao(EspecificacaoProduto especificacao) {
		this.especificacao = especificacao;
	}
}
