package entity;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EspecificacaoPessoa{
	private int id;
	private int parametro;
	private String nome;
	private String cpf;
	private int limite;
	private String dataNascimento;
	private String login;
	private char[] senha;
	private String nivelDeAcesso;
	private String dataInicial;
	private String dataFinal;
	private String rua;
	private String bairro;
	private String estado;
	private String cidade;
	private int numero;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Integer getLimite() {
		return limite;
	}

	public void setLimite(int limite) {
		this.limite = limite;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public char[] getSenha() {
		return senha;
	}

	public void setSenha(char[] senha) {
		this.senha = senha;
	}

	public String getCargo() {
		return nivelDeAcesso;
	}

	public void setCargo(String cargo) {
		this.nivelDeAcesso = cargo;
	}

	public String getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial() {
		Date data = new Date();
		SimpleDateFormat smt = new SimpleDateFormat("DD/MM/YYYY");
		this.dataInicial = smt.format(data);
	}

	public String getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(String string) {
		this.dataFinal = string;
	}

	public int getNumero() {
		return numero;
	}

	public String getEstado() {
		return estado;
	}

	public String getBairro() {
		return bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public String getRua() {
		return rua;
	}

	public void setNumero(int i) {
		this.numero = i;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}
	public void setParametro(EspecificacaoPessoa espec){
		this.parametro = espec.getId();
	}
	public int getParametro(){
		return this.parametro;
	}
}
