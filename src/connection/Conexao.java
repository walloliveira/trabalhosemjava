package connection;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexao {
	private static Conexao instance = new Conexao();
	private Connection conexao;
	static final String USUARIO = "postgres";
	static final String SENHA = "ewqiop321";

	static final String DATABASE_URL = "jdbc:postgresql://localhost:5432/trabalhoJava";

	private Conexao() {
		try {
			Class.forName("org.postgresql.Driver");
			this.conexao = DriverManager.getConnection(DATABASE_URL, USUARIO,
					SENHA);
			this.conexao.setAutoCommit(false);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static Conexao getInstance() {
		return instance;
	}

	public void rollback() throws SQLException {
		conexao.rollback();
	}

	public void commit() throws SQLException {
		conexao.commit();
	}

	public PreparedStatement preparedStatement(String sql) throws SQLException {
		return conexao.prepareStatement(sql);
	}

	public Statement createStatement(int typeScrollInsensitive, int concurUpdatable) throws SQLException {
		return conexao.createStatement(typeScrollInsensitive,concurUpdatable);
	}
}
