package control;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import dao.ProdutoDAO;
import entity.EspecificacaoProduto;
import entity.Pessoa;
import entity.Produto;

public class ProdutoControl {
	private ProdutoDAO produtoDao = new ProdutoDAO();
	private Produto novoProduto = new Produto();
	private ResultSetTableModel tableModel = new ResultSetTableModel();

	public void incluirNovoProduto(EspecificacaoProduto especProd) throws SQLException {
		Produto novoProduto = new Produto();
		novoProduto.setEspecificacao(especProd);
		ProdutoDAO produtoDao = new ProdutoDAO();
		produtoDao.inserirProduto(novoProduto);
	}

	public Produto buscaProduto(Produto buscaProduto) throws SQLException {
		ProdutoDAO buscaProdutoDao = new ProdutoDAO();
		return buscaProdutoDao.buscaProduto(buscaProduto);
	}

	public void deletarProduto(EspecificacaoProduto especProd) throws SQLException {
		this.novoProduto.setEspecificacao(especProd);
		this.produtoDao.deletaProduto(novoProduto);
	}

	public void editaProduto(EspecificacaoProduto novaEspec) throws SQLException {
		this.novoProduto.setEspecificacao(novaEspec);
		this.produtoDao.editaProduto(novoProduto);
		this.tableModel.setQuery();
	}

	public ResultSetTableModel getTableModel() {
		return this.tableModel;
	}
	
	public ArrayList<Produto> getProdutos() throws SQLException {
		ArrayList<Produto> produtos = new  ArrayList<Produto>();
		produtos = this.produtoDao.getProdutos();
		JOptionPane.showMessageDialog(null, produtos.get(0));
		return produtos;
	}
}
