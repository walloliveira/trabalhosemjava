package control;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.table.DefaultTableModel;

import connection.Conexao;

public class ResultSetTableModel extends DefaultTableModel {
	private ResultSet rs;
	private Statement st;
	private ResultSetMetaData rsmd;
	private int numeroDeLinhas;
	
	public ResultSetTableModel() {
	}
	
	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
	
	public Class getColumnClass(int column) throws IllegalStateException{
		String className;
		try {
			className = rsmd.getColumnClassName(column + 1);
			return Class.forName(className);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return Object.class;
	}

	@Override
	public int getRowCount() {
		return numeroDeLinhas;
	}

	@Override
	public int getColumnCount() {
		try {
			return rsmd.getColumnCount();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	public String getColumnName(int column){
		try {
			return rsmd.getColumnName(column + 1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		try {
			rs.absolute(rowIndex + 1);
			return rs.getObject(columnIndex + 1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void setQuery(){
		try {
			this.st = Conexao.getInstance().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			this.rs = this.st.executeQuery("select id as Codigo, descricao, " +
					" codigodebarra, valor from produto order by 1");
			this.rsmd = this.rs.getMetaData();
			this.rs.last();
			this.numeroDeLinhas = this.rs.getRow();
			fireTableDataChanged();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void GetQueryPessoa() {
		try {
			this.st = Conexao.getInstance().createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			this.rs = this.st
					.executeQuery("SELECT * "
							+ "FROM PESSOA, ENDERECO"
							+ "WHERE PESSOA.ID = ENDERECO.ID_PESSOA");
			this.fireTableDataChanged();
			this.rsmd = this.rs.getMetaData();
			this.rs.last();
			this.numeroDeLinhas = this.rs.getRow();
			this.fireTableDataChanged();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void listarPessoa() {
		try {
			this.st = Conexao.getInstance().createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			this.rs = this.st
					.executeQuery("SELECT * "
							+ "FROM PESSOA");
			this.rsmd = this.rs.getMetaData();
			this.rs.last();
			this.numeroDeLinhas = this.rs.getRow();
			this.fireTableDataChanged();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}