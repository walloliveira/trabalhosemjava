package control;

import java.sql.SQLException;
import java.text.ParseException;

import dao.PessoaDAO;
import entity.EspecificacaoPessoa;
import entity.Pessoa;

public class PessoaControl {
	private PessoaDAO pessoaDAO = new PessoaDAO();
	private Pessoa novoPessoa = new Pessoa();

	public void setNovoPessoa(EspecificacaoPessoa especPessoa) throws SQLException {
		novoPessoa.setEspecificacao(especPessoa);
		pessoaDAO.inserirPessoa(novoPessoa);
	}

	public Pessoa buscaUsuario(Pessoa buscaPessoa) throws SQLException{
		return pessoaDAO.buscaPessoa(buscaPessoa);
	}

	public Pessoa deletaUsuario(EspecificacaoPessoa especPessoa) throws SQLException{
		novoPessoa.setEspecificacao(especPessoa);
		return pessoaDAO.deletaPessoa(novoPessoa);
	}

	public void editaPessoa(EspecificacaoPessoa novaEspec) throws SQLException, ParseException {
		this.novoPessoa.setEspecificacao(novaEspec);
		this.pessoaDAO.editaPessoa(novoPessoa);
	}
	
	public Pessoa validaLogin(EspecificacaoPessoa especUsuario) throws SQLException {
		Pessoa loginUsuario = new Pessoa();
		loginUsuario.setEspecificacao(especUsuario);
		return this.pessoaDAO.validaLogin(loginUsuario);
	}

	public String retornaNivelDeAcesso(Pessoa pessoa) throws SQLException {
		return this.pessoaDAO.retornaNivelAcesso(pessoa);
		
	}
}
