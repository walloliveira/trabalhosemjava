package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLDataException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import connection.Conexao;
import entity.EspecificacaoPessoa;
import entity.Pessoa;

public class PessoaDAO {
	private PreparedStatement ps = null;

	public void inserirPessoa(Pessoa novaPessoa) throws SQLException {
		ps = Conexao
				.getInstance()
				.preparedStatement(
						"INSERT INTO "
								+ "PESSOA "
								+ "(NOME, DATA_NASCIMENTO, CPF, LIMITE_CREDITO, LOGIN, SENHA, DATA_INICIAL, DATA_FINAL, nivel_acesso, RUA, CIDADE, ESTADO, NUMERO, BAIRRO)"
								+ "VALUES "
								+ "(?, to_date(?,'DD/MM/YYYY'),?,?,?,?,to_date(?,'DD/MM/YYYY'),to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?)");
		ps.setString(1, novaPessoa.getEspecificacao().getNome());
		ps.setString(2, novaPessoa.getEspecificacao().getDataNascimento());
		ps.setString(3, novaPessoa.getEspecificacao().getCpf());
		ps.setInt(4, novaPessoa.getEspecificacao().getLimite());
		ps.setString(5, novaPessoa.getEspecificacao().getLogin());
		ps.setString(6,
				String.valueOf(novaPessoa.getEspecificacao().getSenha()));
		ps.setString(7, novaPessoa.getEspecificacao().getDataInicial());
		ps.setString(8, novaPessoa.getEspecificacao().getDataFinal());
		ps.setString(9,
				String.valueOf(novaPessoa.getEspecificacao().getCargo()));
		ps.setString(10, novaPessoa.getEspecificacao().getRua());
		ps.setString(11, novaPessoa.getEspecificacao().getCidade());
		ps.setString(12, novaPessoa.getEspecificacao().getEstado());
		ps.setInt(13,
				Integer.valueOf(novaPessoa.getEspecificacao().getNumero()));
		ps.setString(14, novaPessoa.getEspecificacao().getBairro());

		ps.executeUpdate();

		try {
			Conexao.getInstance().commit();
		} catch (SQLException e) {
			try {
				Conexao.getInstance().rollback();
			} catch (SQLException e1) {

			}
		}
	}

	public Pessoa buscaPessoa(Pessoa buscaPessoa) throws SQLException {

		ResultSet rs = null;
		ps = Conexao.getInstance().preparedStatement(
				"SELECT * FROM  PESSOA WHERE CPF = (?)");
		ps.setString(1, buscaPessoa.getEspecificacao().getCpf());
		rs = ps.executeQuery();
		while (rs.next()) {
			Pessoa retornaUsuario = new Pessoa();
			EspecificacaoPessoa especificacao = new EspecificacaoPessoa();
			especificacao.setId(rs.getInt("id"));
			especificacao.setNome(rs.getString("nome"));
			especificacao.setDataNascimento(rs.getString("data_nascimento"));
			especificacao.setCpf(rs.getString("CPF"));
			especificacao.setLimite(rs.getInt("limite_credito"));
			especificacao.setLogin(rs.getString("login"));
			especificacao.setSenha(rs.getString("senha").toCharArray());
			especificacao
					.setDataFinal(String.valueOf(rs.getDate("data_final")));
			especificacao.setCargo(rs.getString("nivel_acesso"));
			especificacao.setRua(rs.getString("rua"));
			especificacao.setCidade(rs.getString("cidade"));
			especificacao.setEstado(rs.getString("estado"));
			especificacao.setNumero(rs.getInt("numero"));
			especificacao.setBairro(rs.getString("bairro"));

			retornaUsuario.setEspecificacao(especificacao);
			return retornaUsuario;
		}
		return null;
	}

	public Pessoa deletaPessoa(Pessoa pessoa) throws SQLException {
		if (buscaPessoa(pessoa) == null) {
			return null;
		}
		try {
			ps = Conexao.getInstance().preparedStatement(
					"DELETE FROM PESSOA WHERE CPF = (?)");
			ps.setString(1, pessoa.getEspecificacao().getCpf());
			ps.executeUpdate();
			Conexao.getInstance().commit();
		} catch (SQLException e) {
			try {
				Conexao.getInstance().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public Pessoa validaLogin(Pessoa loginUsuario) throws SQLException {
		ResultSet rs = null;
		ps = Conexao.getInstance().preparedStatement(
				"SELECT * FROM PESSOA WHERE LOGIN = ? AND SENHA = ?");
		ps.setString(1, loginUsuario.getEspecificacao().getLogin());
		ps.setString(2,
				String.valueOf(loginUsuario.getEspecificacao().getSenha()));
		rs = ps.executeQuery();

		if (!rs.next()) {
			throw new SQLDataException("Erro de Login!");
		}

		return loginUsuario;
	}

	public Date converte(String dataNasc) throws ParseException {
		DateFormat forma = new SimpleDateFormat("dd/MM/yyyy");
		java.sql.Date data = new java.sql.Date(forma.parse(dataNasc).getTime());

		return data;
	}

	public void editaPessoa(Pessoa novaPessoa) throws SQLException,
			ParseException {
		ps = Conexao
				.getInstance()
				.preparedStatement(
						"UPDATE PESSOA SET (NOME, DATA_NASCIMENTO, LIMITE_CREDITO, LOGIN, SENHA, DATA_INICIAL, DATA_FINAL, nivel_acesso, RUA, CIDADE, ESTADO, NUMERO, BAIRRO)"
								+ "VALUES "
								+ "(?, to_date(?,'DD/MM/YYYY'),?,?,?,to_date(?,'DD/MM/YYYY'),to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?) WHERE ID = ?)"
								+ "ORDER BY 1");
		ps.setString(1, novaPessoa.getEspecificacao().getNome());
		ps.setString(2, novaPessoa.getEspecificacao().getDataNascimento());

		ps.setInt(3
				, novaPessoa.getEspecificacao().getLimite());
		ps.setString(4, novaPessoa.getEspecificacao().getLogin());
		ps.setString(5,
				String.valueOf(novaPessoa.getEspecificacao().getSenha()));
		ps.setString(6, novaPessoa.getEspecificacao().getDataInicial());
		ps.setString(7, novaPessoa.getEspecificacao().getDataFinal());
		ps.setString(8,
				String.valueOf(novaPessoa.getEspecificacao().getCargo()));
		ps.setString(9, novaPessoa.getEspecificacao().getRua());
		ps.setString(10, novaPessoa.getEspecificacao().getCidade());
		ps.setString(11, novaPessoa.getEspecificacao().getEstado());
		ps.setInt(12,
				Integer.valueOf(novaPessoa.getEspecificacao().getNumero()));
		ps.setString(13, novaPessoa.getEspecificacao().getBairro());
		ps.setInt(14, novaPessoa.getEspecificacao().getParametro());
		ps.executeUpdate();
		try {
			ps.executeUpdate();
		} catch (SQLException e) {
			Conexao.getInstance().rollback();
		}
		try {
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String retornaNivelAcesso(Pessoa pessoa) throws SQLException {
		ps = Conexao.getInstance().preparedStatement(
				"SELECT NIVEL_ACESSO FROM PESSOA WHERE LOGIN = ?");
		ps.setString(1, pessoa.getEspecificacao().getLogin());
		ResultSet rs = null;
		rs = ps.executeQuery();
		if (!rs.next()) {
			throw new SQLDataException("Erro");
		}
		return rs.getString("NIVEL_ACESSO");
	}
}