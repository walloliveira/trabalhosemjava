package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import connection.Conexao;
import entity.EspecificacaoProduto;
import entity.Produto;

public class ProdutoDAO {
	private PreparedStatement ps = null;

	public void inserirProduto(Produto novoProduto) throws SQLException{

		buscaProduto(novoProduto);
			ps = Conexao.getInstance().preparedStatement(
					"INSERT INTO " + "PRODUTO "
							+ "(ID, DESCRICAO, CODIGODEBARRA, VALOR)"
							+ "VALUES " + "(?,?,?,?)");
			ps.setInt(1, novoProduto.getEspecificacao().getId());
			ps.setString(2, novoProduto.getEspecificacao().getDescricao());
			ps.setString(3, novoProduto.getEspecificacao().getCodigoDeBarra());
			ps.setBigDecimal(4, novoProduto.getEspecificacao().getValor());
			ps.executeUpdate();
		
			try {
				Conexao.getInstance().commit();
			} catch (SQLException e1) {
				Conexao.getInstance().rollback();
				throw new SQLException();
			}
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
}
	public Produto buscaProduto(Produto buscaProduto) throws SQLException{

		ResultSet rs = null;
			ps = Conexao.getInstance().preparedStatement(
					"SELECT * FROM  PRODUTO WHERE ID = (?)");
			ps.setInt(1, buscaProduto.getEspecificacao().getId());
			rs = ps.executeQuery();
			while (rs.next()) {
				Produto retornaProduto = new Produto();
				EspecificacaoProduto especificacao = new EspecificacaoProduto();
				especificacao.setId(rs.getInt("id"));
				especificacao.setDescricao(rs.getString("descricao"));
				especificacao.setCodigoDeBarra(rs.getString("codigodebarra"));
				especificacao.setValor(rs.getBigDecimal("valor"));
				retornaProduto.setEspecificacao(especificacao);
				return retornaProduto;
			}
			rs.close();
		return null;
	}

	public void deletaProduto(Produto produto) throws SQLException{
		buscaProduto(produto);
		ps = Conexao.getInstance().preparedStatement(
				"DELETE FROM PRODUTO WHERE ID = (?)");
		ps.setInt(1, produto.getEspecificacao().getId());
		ps.executeUpdate();
		try {
			Conexao.getInstance().commit();
		} catch (SQLException e1) {
			Conexao.getInstance().rollback();
			throw new SQLException();
		}
		try {
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void editaProduto(Produto novoProduto) throws SQLException {
		ps = Conexao.getInstance().preparedStatement("UPDATE PRODUTO SET DESCRICAO = ?, CODIGODEBARRA = ?, VALOR = ? WHERE ID = ?");
		ps.setString(1, novoProduto.getEspecificacao().getDescricao());
		ps.setString(2, novoProduto.getEspecificacao().getCodigoDeBarra());
		ps.setBigDecimal(3,novoProduto.getEspecificacao().getValor());
		ps.setInt(4, novoProduto.getEspecificacao().getId());
		ps.executeUpdate();
		try {
			Conexao.getInstance().commit();
		} catch (SQLException e1) {
			Conexao.getInstance().rollback();
			throw new SQLException();
		}
		try {
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public ArrayList<Produto> getProdutos() throws SQLException {
		ps = Conexao.getInstance().preparedStatement("SELECT * FROM PRODUTO");
		ResultSet rs = null;
		rs = ps.executeQuery();
		ArrayList<Produto> produtos = new ArrayList<Produto>();
		while(!rs.next()){
			EspecificacaoProduto espec = new EspecificacaoProduto();
			Produto produto = new Produto();
			espec.setId(rs.getInt("ID"));
			espec.setDescricao(rs.getString("DESCRICAO"));
			espec.setCodigoDeBarra(rs.getString("CODIGODEBARRA"));
			espec.setValor(rs.getBigDecimal("VALOR"));
			produto.setEspecificacao(espec);
			produtos.add(produto);
		}
		return produtos;
	}
}
